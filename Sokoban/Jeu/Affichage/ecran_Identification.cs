using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Sokoban.Jeu.Action;

namespace Sokoban.Jeu.Affichage
{
    public partial class ecran_Identification : UserControl
    {
        private JeuSokoban jeuSokoban;

        /// <summary>
        /// Initialisation de la page d'Identification
        /// </summary>
        /// <param name="jeuSokoban">Jeu principal</param>
        public ecran_Identification(JeuSokoban jeuSokoban)
        {
            this.jeuSokoban = jeuSokoban;

            InitializeComponent();
            
            // R�cup�ration de liste de joueurs 
            List<String> joueurs = Joueur.nomsJoueurs();
            foreach(String j in joueurs) {
                Lsb_Comptes.Items.AddRange(new object[] { j });
            }
            
            // Affichage du joueur en cours
            if (jeuSokoban.JoueurActif != null)
            {
                lbl_Actif_NomJoueur.Text = jeuSokoban.JoueurActif.NomJoueur;
                //bt_Activer.Enabled = true;
                //bt_Supprimer.Enabled = true;
            }

            // Modification d'affichage d'�lement en fonction de la page suivante
            switch (jeuSokoban.EcranSuivant)
            {
                case JeuSokoban.DIFFICULTE :
                        bt_Joueur.Visible = true;
                        bt_Joueur.Enabled = false;
                    break;

            }

        }

        /// <summary>
        /// Action sur le champs pour inscrire son nom
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txb_Creer_TextChanged(object sender, EventArgs e)
        {
            if (!txb_Creer.Text.Equals(""))
            {
                // tester si le nom existe
                //
                //  � FAIRE
                //
                //
                //
                //
                
            }
        }

        /// <summary>
        /// Losque que l'on presse le bouton Creer pour ajouter un compte � la base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void bt_Creer_Click(object sender, EventArgs e)
        {
            if (!txb_Creer.Text.Equals(""))
            {
                try
                {
                    // Demande de cr�ation de joueur
                    Joueur joueur = new Joueur(txb_Creer.Text.ToString());
                    // Placer le joueur dans la liste des joueurs
                    Joueur.AddJoueur(txb_Creer.Text.ToString(), joueur);
                    // Ajout de son nom � la liste
                    Lsb_Comptes.Items.AddRange(new object[] {txb_Creer.Text.ToString()});
                    // effacer le comptenu du champ Cr�er
                    txb_Creer.Text = "";
                }
                catch 
                {
                    MessageBox.Show("V�rifiez que ce nom n'existe pas.");
                }
            }
        }


        /// <summary>
        /// Losque l'on selectionne un des champs dans dans la liste
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Lsb_Comptes_SelectedIndexChanged(object sender, EventArgs e)
        {
            bt_Activer.Enabled = true;
            //bt_Supprimer.Enabled = true;
        }

        private void bt_Activer_Click(object sender, EventArgs e)
        {
            String nomJoueur;

            for (int i = 0; i < Lsb_Comptes.Items.Count; i++)
            {
                if (Lsb_Comptes.GetSelected(i) == true)
                {
                    nomJoueur = Lsb_Comptes.SelectedItem.ToString();
                    lbl_Actif_NomJoueur.Text = nomJoueur;
                    jeuSokoban.activerJoueur(nomJoueur);
                }
            }

            switch (jeuSokoban.EcranSuivant)
            {
                case JeuSokoban.DIFFICULTE:

                    if (jeuSokoban.JoueurActif != null)
                    {
                        System.Console.WriteLine(jeuSokoban.JoueurActif.NomJoueur);
                        bt_Joueur.Enabled = true;
                    }
                    break;

            }
        }

        private void bt_Supprimer_Click(object sender, EventArgs e)
        {

        }

        private void bt_Retour_Click(object sender, EventArgs e)
        {
            jeuSokoban.ecran(JeuSokoban.MENU_PRINCIPAL);
        }

        private void bt_Jouer_Click(object sender, EventArgs e)
        {
            jeuSokoban.ecran(JeuSokoban.DIFFICULTE);
        }

    
    }
}
