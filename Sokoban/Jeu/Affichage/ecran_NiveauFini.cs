using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Sokoban.Jeu.Niveau;
using System.IO;

namespace Sokoban.Jeu.Affichage
{
    public partial class ecran_NiveauFini : UserControl
    {
        public JeuSokoban jeuSokoban;

        private int ligneSuiv;
        private int niveauSuiv;
        private int lignePrec;
        private int niveauPrec;

        public ecran_NiveauFini(JeuSokoban jeuSokoban)
        {
            this.jeuSokoban = jeuSokoban;

            NivInfos nivInfo = jeuSokoban.NiveauInfos;

            InitializeComponent();
            
            // Affichage des infos du joueur
            int courant = nivInfo.Temps;
            String TimeInString;
            int courantMinute = courant / 60000;
            int courantSeconde = (courant % 60000) / 1000;

            TimeInString = String.Format("{0:D2}", courantMinute); 
            TimeInString += ":" +  String.Format("{0:D2}", courantSeconde);

            lb_Temps.Text = TimeInString;
            lb_Mouvement.Text = String.Format("{0:D4}", nivInfo.Deplacements);

            jeuSokoban.JoueurActif.AddScore(nivInfo);

            // désactivation de bouton

            testNiveau(-1, bt_NivPrec, lb_TextPrec);
            testNiveau(0, bt_Recommencer, lb_Rec);
            testNiveau(1, bt_NivSuiv, lb_Suiv);
        }

        private void testNiveau(int i, Button bouton, Label label)
        {
            int ligne = jeuSokoban.NiveauInfos.jeuLigne;
            int niveau = jeuSokoban.NiveauInfos.jeuNiveau + i %10;
            niveau = (niveau == 0) ? 10 : niveau;
            if (i >= 0)
            {
                ligne += ((jeuSokoban.NiveauInfos.jeuNiveau + i % 10 == 0) ? 1 : 0);
                ligneSuiv = ligne;
                niveauSuiv = niveau;
            }
            else if (i <= 0)
            {
                ligne += ((jeuSokoban.NiveauInfos.jeuNiveau + i % 10 == 0) ? -1 : 0);
                lignePrec = ligne;
                niveauPrec = niveau;
            }
            //Fichier test = new Fichier(jeuSokoban.NiveauInfos.jeuNom, ligne+" - "+niveau+".xsb");
            bool isExist = File.Exists(@"Niveaux\" + jeuSokoban.NiveauInfos.jeuNom +
                                       @"\" + (ligne-1) + "-" + (niveau-1) + ".xsb");

            bouton.Visible = isExist;
            label.Visible = isExist;
            if (isExist) 
            {
                String nom;
                if (jeuSokoban.NiveauInfos.jeuNom.Equals("defaut"))
                {
                    nom = "Niveau " + ligne + "-" + niveau;
                }
                else
                {
                    nom = jeuSokoban.NiveauInfos.jeuNom + "  " + ligne + "-" + niveau;
                }

                label.Text = nom;

            } 
        }


        private void bt_NivPrec_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau(jeuSokoban.NiveauInfos.jeuNom, lignePrec, niveauPrec);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

        private void bt_Recommencer_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau(jeuSokoban.NiveauInfos.jeuNom, jeuSokoban.NiveauInfos.jeuLigne, jeuSokoban.NiveauInfos.jeuNiveau);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

        private void bt_NivSuiv_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau(jeuSokoban.NiveauInfos.jeuNom, ligneSuiv, niveauSuiv);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

        private void bt_MenuPrincipal_Click(object sender, EventArgs e)
        {
            jeuSokoban.ecran(JeuSokoban.MENU_PRINCIPAL);
        }


    }
}
