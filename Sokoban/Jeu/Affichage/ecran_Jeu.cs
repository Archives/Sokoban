using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Sokoban.Jeu.Niveau;

namespace Sokoban.Jeu.Affichage
{
    public partial class ecran_Jeu : UserControl
    {

        public const int MUR = 0;
        public const int SOL = 1;
        public const int ARRIVEE = 2;

        public const int RIEN = 0;
        public const int CAISSE = 1;

        private int[,] niveau;
        private int[,] niveauElements;
        private int _xpas = 20;
        private int _ypas = 20;
        private int _x = 0;
        private int _y = 0;
        private int _xpos = 1;
        private int _ypos = 1;
        private int mouvements = 0;
        private int nombreCaisse = 0;
        private int[] etatCaisse;
        private bool fin;
        private String appPath = Application.StartupPath;
        private System.Windows.Forms.PictureBox[] caisses;

        private ecran_Niveau eNiv;

        public ecran_Jeu(ecran_Niveau eNiv, NivInfos nivInfo)
        {
            this.eNiv = eNiv;
            
            this.Focus();
            
            InitializeComponent();
            Fichier niveauCharger = nivInfo.terrain;
            niveau = niveauCharger.getNiveau();
            niveauElements = niveauCharger.getNiveauElements();
            
            int[] joueur = niveauCharger.getJoueur();
            _xpos = joueur[1];
            _ypos = joueur[0];
            _x = _xpos * _xpas;
            _y = _ypos * _ypas;

            personnage.ImageLocation = (@"Images\perso_gauche.gif");
            personnage.Location = new System.Drawing.Point(_x, _y);
            personnage.BackColor = Color.Transparent;
            personnage.Size = new System.Drawing.Size(_xpas, _ypas);

            int nombreCaseX = niveau.GetLength(1);
            int nombreCaseY = niveau.GetLength(0);

            // afficher les caisses
            nombreCaisse = 0;
            for (int i = 0; i < nombreCaseY; i++)
            {
                for (int j = 0; j < nombreCaseX; j++)
                {
                    nombreCaisse += niveauElements[i, j];
                    if (niveauElements[i, j] > RIEN)
                    {
                        niveauElements[i, j] = nombreCaisse; // on place un identifateur � la place
                    }
                }
            }
            etatCaisse = new int[nombreCaisse];

            caisses = new System.Windows.Forms.PictureBox[nombreCaisse+1];
            int k = 1;
            for (int i = 0; i < nombreCaseY; i++)
            {
                for (int j = 0; j < nombreCaseX; j++)
                {
                    if (niveauElements[i, j] > RIEN)
                    {
                        caisses[k] = new System.Windows.Forms.PictureBox();
                        caisses[k].Size = new System.Drawing.Size(_xpas, _ypas);
                        if (niveau[i, j] == ARRIVEE) 
                        {
                            caisses[k].ImageLocation = (@"Images\caisse_placee.png");
                            etatCaisse[k - 1] = 1;
                        } 
                        else 
                        {
                            caisses[k].ImageLocation = (@"Images\caisse.png");
                        }
                        caisses[k].Location = new System.Drawing.Point(j * _ypas, i * _xpas);
                        Controls.Add(caisses[k]);
                        k++;
                    }
                }
            }

            // changer une sol et mur
            

            System.Windows.Forms.PictureBox[,] sol;
            sol = new System.Windows.Forms.PictureBox[nombreCaseY,nombreCaseX];
            
            for (int i = 0; i < nombreCaseY; i++) 
            {
                for (int j = 0; j < nombreCaseX; j++) 
                {

                    sol[i,j] = new System.Windows.Forms.PictureBox();
                    sol[i,j].Size = new System.Drawing.Size(_xpas, _ypas);
                    switch (niveau[i, j]) {
                        case SOL :
                            sol[i, j].ImageLocation = (@"Images\sol.png");
                            break;
                        case MUR:
                            sol[i, j].ImageLocation = (@"Images\mur.png");
                            break;
                        case ARRIVEE :
                            sol[i, j].ImageLocation = (@"Images\zone.png");
                            break;
                    }
                      
                    
                   
                    sol[i,j].Location = new System.Drawing.Point(j*_ypas,i*_xpas);
                    Controls.Add(sol[i,j]);
         
                }
            }
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Right: 
                    deplacement(1, 0);
                    personnage.ImageLocation = (@"Images\perso_droite.gif");
                    return true;
                case Keys.Left: 
                    deplacement(-1, 0);
                    personnage.ImageLocation = (@"Images\perso_gauche.gif");
                    return true;
                case Keys.Up: 
                    deplacement(0, -1);
                    personnage.ImageLocation = (@"Images\perso_haut.gif");
                    return true;
                case Keys.Down:
                    deplacement(0, 1);
                    personnage.ImageLocation = (@"Images\perso_bas.gif");
                    return true;
                default: return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        public void deplacement(int x, int y)
        {
            if (testDeplacement(x, y))
            {
                if (testCaisse(x, y))
                {
                    int id = niveauElements[_ypos + y, _xpos + x];
                    caisses[id].Location = new System.Drawing.Point((x * 2 + _xpos) * _xpas, (y * 2 + _ypos) * _ypas);
                    if (testCaissePlacee(x * 2, y * 2))
                    {
                        caisses[id].ImageLocation = (@"Images\caisse_placee.png");
                        etatCaisse[id - 1] = 1;

                    }
                    else
                    {
                        caisses[id].ImageLocation = (@"Images\caisse.png");
                        etatCaisse[id - 1] = 0;
                    }

                    niveauElements[_ypos + 2 * y, _xpos + 2 * x] = niveauElements[_ypos + y, _xpos + x];
                    niveauElements[_ypos + y, _xpos + x] = 0;

                    undoAdd(_xpos, _ypos, x, y, id);
                    testFin();
                }
                else
                {
                    undoAdd(_xpos, _ypos, x, y);
                }

                
                _xpos += x;
                _ypos += y;
                _x += x * _xpas;
                _y += y * _ypas;
                mouvements++;
                eNiv.mouvementChange(mouvements);
                personnage.Location = new System.Drawing.Point(_x, _y);
                
            }          
        }

        private List<int[]> undoListe = new List<int[]>();


        private void undoAdd(int xpos, int ypos, int x, int y)
        {
            int[] l = {xpos, ypos, x, y, 0};
            undoListe.Add(l);
        }

        private void undoAdd(int xpos, int ypos, int x, int y, int id)
        {
            int[] l = {xpos, ypos, x, y, id};
            undoListe.Add(l);
        }

        public void undo()
        {
            if (mouvements > 0)
            {
                int[] l = undoListe[undoListe.Count - 1];            
                undoListe.Remove(l);


                if (l[2] == 1 && l[3] == 0)
                {
                    personnage.ImageLocation = (@"Images\perso_droite.gif");
                }
                if (l[2] == -1 && l[3] == 0)
                {
                    personnage.ImageLocation = (@"Images\perso_gauche.gif");
                }
                if (l[2] == 0 && l[3] == -1)
                {
                    personnage.ImageLocation = (@"Images\perso_haut.gif");
                }
                if (l[2] == 0 && l[3] == 1)
                {
                    personnage.ImageLocation = (@"Images\perso_bas.gif");
                }
               

                if (l[4] > 0)
                {
                    caisses[l[4]].Location = new System.Drawing.Point(_xpos * _xpas,  _ypos * _ypas);
                    if (testCaissePlacee(l[2], l[3]))
                    {
                        caisses[l[4]].ImageLocation = (@"Images\caisse_placee.png");
                        etatCaisse[l[4] - 1] = 1;

                    }
                    else
                    {
                        caisses[l[4]].ImageLocation = (@"Images\caisse.png");
                        etatCaisse[l[4] - 1] = 0;
                    }

                    niveauElements[_ypos, _xpos] = niveauElements[l[0], l[1]];
                    niveauElements[l[0], l[1]] = 0;
                }

                _xpos = l[0];
                _ypos = l[1];
                _x = l[0] * _xpas;
                _y = l[1] * _ypas;
                mouvements--;
                eNiv.mouvementChange(mouvements);
                personnage.Location = new System.Drawing.Point(_x, _y);
            }
           

        }


        public void testFin()
        {
            int compteur = 0;
            for (int i = 0; i < nombreCaisse; i++)
            {
                compteur += etatCaisse[i];
            }
            fin = (compteur == nombreCaisse) ? true : false;
            if (fin)
            {
                eNiv.fin(mouvements);
            }
        }

        public bool testDeplacement(int x, int y)
        {
            /* 0 = mur
             * 1 = sol
             * 2 = arriv�es
             */

            if (niveau[ _ypos + y, _xpos + x] > MUR)
            {
                
                // test si il a une caisse
                if (!testCaisse(x, y) || (testCaisse(x, y) && !testCaisse(2 * x, 2 * y) && niveau[_ypos + 2 * y, _xpos + 2 * x] >= 1))
                {
                    return true;
                } 
                else 
                {
                    return false;
                }                 
            }
            else
            {
                return false;
            }
        }

        public bool testCaisse(int x, int y)
        {
            /* 0 = pas caisse
             * 1 = caisse
             */
           
            if (niveauElements[_ypos + y, _xpos + x] > RIEN)
            {
                return true;                
            }
            else
            {
                return false;
            }
        }

        public bool testCaissePlacee(int x, int y)
        {
            if (niveau[_ypos + y, _xpos + x] == ARRIVEE)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
