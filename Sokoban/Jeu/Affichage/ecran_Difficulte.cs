using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Sokoban.Jeu.Affichage
{
    public partial class ecran_Difficulte : UserControl
    {
        private JeuSokoban jeuSokoban;

        public ecran_Difficulte(JeuSokoban jeuSokoban)
        {
            this.jeuSokoban = jeuSokoban;
            InitializeComponent();

            if (jeuSokoban.JoueurActif.NomCourant == null)
            {
                bt_Reprendre.Visible = false;
            }
        }

        private void bt_lv1_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau("defaut", 1, 1);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

        private void bt_lv2_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau("defaut", 3, 1);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

        private void bt_lvl3_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau("defaut", 5, 1);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

        private void bt_Retour_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.ecran(JeuSokoban.MENU_PRINCIPAL);
        }

        private void bt_Reprendre_Click(object sender, EventArgs e)
        {

            int ligne = jeuSokoban.JoueurActif.LigneCourant;
            int niveau = (jeuSokoban.JoueurActif.NivCourant + 1) % 10;
            niveau = (niveau == 0) ? 10 : niveau;
            ligne += ((jeuSokoban.JoueurActif.LigneCourant + 1 % 10 == 0) ? 1 : 0);
            
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau(jeuSokoban.JoueurActif.NomCourant, ligne, niveau);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

      
    }
}
