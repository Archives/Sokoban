﻿namespace Sokoban.Jeu.Affichage
{
    partial class ecran_NiveauFini
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_Titre = new System.Windows.Forms.Label();
            this.gbx_Actu = new System.Windows.Forms.GroupBox();
            this.lb_Mouvement = new System.Windows.Forms.Label();
            this.lb_MouvementTitre = new System.Windows.Forms.Label();
            this.lb_Temps = new System.Windows.Forms.Label();
            this.lb_TempsTitre = new System.Windows.Forms.Label();
            this.bt_MenuPrincipal = new System.Windows.Forms.Button();
            this.bt_Recommencer = new System.Windows.Forms.Button();
            this.bt_NivSuiv = new System.Windows.Forms.Button();
            this.bt_NivPrec = new System.Windows.Forms.Button();
            this.lb_TextPrec = new System.Windows.Forms.Label();
            this.lb_Rec = new System.Windows.Forms.Label();
            this.lb_Suiv = new System.Windows.Forms.Label();
            this.gbx_Actu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lb_Titre
            // 
            this.lb_Titre.AutoSize = true;
            this.lb_Titre.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titre.Location = new System.Drawing.Point(256, 62);
            this.lb_Titre.Name = "lb_Titre";
            this.lb_Titre.Size = new System.Drawing.Size(116, 25);
            this.lb_Titre.TabIndex = 0;
            this.lb_Titre.Text = "Félicitation";
            // 
            // gbx_Actu
            // 
            this.gbx_Actu.Controls.Add(this.lb_Mouvement);
            this.gbx_Actu.Controls.Add(this.lb_MouvementTitre);
            this.gbx_Actu.Controls.Add(this.lb_Temps);
            this.gbx_Actu.Controls.Add(this.lb_TempsTitre);
            this.gbx_Actu.Location = new System.Drawing.Point(229, 117);
            this.gbx_Actu.Name = "gbx_Actu";
            this.gbx_Actu.Size = new System.Drawing.Size(168, 70);
            this.gbx_Actu.TabIndex = 1;
            this.gbx_Actu.TabStop = false;
            this.gbx_Actu.Text = "Score";
            // 
            // lb_Mouvement
            // 
            this.lb_Mouvement.AutoSize = true;
            this.lb_Mouvement.Location = new System.Drawing.Point(86, 41);
            this.lb_Mouvement.Name = "lb_Mouvement";
            this.lb_Mouvement.Size = new System.Drawing.Size(34, 13);
            this.lb_Mouvement.TabIndex = 0;
            this.lb_Mouvement.Text = "00:00";
            // 
            // lb_MouvementTitre
            // 
            this.lb_MouvementTitre.AutoSize = true;
            this.lb_MouvementTitre.Location = new System.Drawing.Point(6, 41);
            this.lb_MouvementTitre.Name = "lb_MouvementTitre";
            this.lb_MouvementTitre.Size = new System.Drawing.Size(68, 13);
            this.lb_MouvementTitre.TabIndex = 0;
            this.lb_MouvementTitre.Text = "Mouvements";
            // 
            // lb_Temps
            // 
            this.lb_Temps.AutoSize = true;
            this.lb_Temps.Location = new System.Drawing.Point(86, 16);
            this.lb_Temps.Name = "lb_Temps";
            this.lb_Temps.Size = new System.Drawing.Size(31, 13);
            this.lb_Temps.TabIndex = 0;
            this.lb_Temps.Text = "0000";
            // 
            // lb_TempsTitre
            // 
            this.lb_TempsTitre.AutoSize = true;
            this.lb_TempsTitre.Location = new System.Drawing.Point(6, 16);
            this.lb_TempsTitre.Name = "lb_TempsTitre";
            this.lb_TempsTitre.Size = new System.Drawing.Size(39, 13);
            this.lb_TempsTitre.TabIndex = 0;
            this.lb_TempsTitre.Text = "Temps";
            // 
            // bt_MenuPrincipal
            // 
            this.bt_MenuPrincipal.Location = new System.Drawing.Point(258, 256);
            this.bt_MenuPrincipal.Name = "bt_MenuPrincipal";
            this.bt_MenuPrincipal.Size = new System.Drawing.Size(119, 23);
            this.bt_MenuPrincipal.TabIndex = 2;
            this.bt_MenuPrincipal.Text = "Menu Principal";
            this.bt_MenuPrincipal.UseVisualStyleBackColor = true;
            this.bt_MenuPrincipal.Click += new System.EventHandler(this.bt_MenuPrincipal_Click);
            // 
            // bt_Recommencer
            // 
            this.bt_Recommencer.Location = new System.Drawing.Point(274, 208);
            this.bt_Recommencer.Name = "bt_Recommencer";
            this.bt_Recommencer.Size = new System.Drawing.Size(87, 23);
            this.bt_Recommencer.TabIndex = 3;
            this.bt_Recommencer.Text = "Recommencer";
            this.bt_Recommencer.UseVisualStyleBackColor = true;
            this.bt_Recommencer.Click += new System.EventHandler(this.bt_Recommencer_Click);
            // 
            // bt_NivSuiv
            // 
            this.bt_NivSuiv.Location = new System.Drawing.Point(398, 208);
            this.bt_NivSuiv.Name = "bt_NivSuiv";
            this.bt_NivSuiv.Size = new System.Drawing.Size(128, 23);
            this.bt_NivSuiv.TabIndex = 2;
            this.bt_NivSuiv.Text = "Niveau suivant →";
            this.bt_NivSuiv.UseVisualStyleBackColor = true;
            this.bt_NivSuiv.Click += new System.EventHandler(this.bt_NivSuiv_Click);
            // 
            // bt_NivPrec
            // 
            this.bt_NivPrec.Location = new System.Drawing.Point(101, 208);
            this.bt_NivPrec.Name = "bt_NivPrec";
            this.bt_NivPrec.Size = new System.Drawing.Size(129, 23);
            this.bt_NivPrec.TabIndex = 2;
            this.bt_NivPrec.Text = "← Niveau précédent";
            this.bt_NivPrec.UseVisualStyleBackColor = true;
            this.bt_NivPrec.Click += new System.EventHandler(this.bt_NivPrec_Click);
            // 
            // lb_TextPrec
            // 
            this.lb_TextPrec.AutoSize = true;
            this.lb_TextPrec.Location = new System.Drawing.Point(146, 234);
            this.lb_TextPrec.Name = "lb_TextPrec";
            this.lb_TextPrec.Size = new System.Drawing.Size(29, 13);
            this.lb_TextPrec.TabIndex = 4;
            this.lb_TextPrec.Text = "Préc";
            this.lb_TextPrec.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lb_Rec
            // 
            this.lb_Rec.AutoSize = true;
            this.lb_Rec.Location = new System.Drawing.Point(291, 234);
            this.lb_Rec.Name = "lb_Rec";
            this.lb_Rec.Size = new System.Drawing.Size(44, 13);
            this.lb_Rec.TabIndex = 4;
            this.lb_Rec.Text = "Courant";
            this.lb_Rec.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lb_Suiv
            // 
            this.lb_Suiv.AutoSize = true;
            this.lb_Suiv.Location = new System.Drawing.Point(452, 234);
            this.lb_Suiv.Name = "lb_Suiv";
            this.lb_Suiv.Size = new System.Drawing.Size(28, 13);
            this.lb_Suiv.TabIndex = 4;
            this.lb_Suiv.Text = "Suiv";
            this.lb_Suiv.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ecran_NiveauFini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lb_Suiv);
            this.Controls.Add(this.lb_Rec);
            this.Controls.Add(this.lb_TextPrec);
            this.Controls.Add(this.bt_Recommencer);
            this.Controls.Add(this.bt_NivPrec);
            this.Controls.Add(this.bt_NivSuiv);
            this.Controls.Add(this.bt_MenuPrincipal);
            this.Controls.Add(this.gbx_Actu);
            this.Controls.Add(this.lb_Titre);
            this.Name = "ecran_NiveauFini";
            this.Size = new System.Drawing.Size(650, 404);
            this.gbx_Actu.ResumeLayout(false);
            this.gbx_Actu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_Titre;
        private System.Windows.Forms.GroupBox gbx_Actu;
        private System.Windows.Forms.Label lb_Mouvement;
        private System.Windows.Forms.Label lb_MouvementTitre;
        private System.Windows.Forms.Label lb_Temps;
        private System.Windows.Forms.Label lb_TempsTitre;
        private System.Windows.Forms.Button bt_MenuPrincipal;
        private System.Windows.Forms.Button bt_Recommencer;
        private System.Windows.Forms.Button bt_NivSuiv;
        private System.Windows.Forms.Button bt_NivPrec;
        private System.Windows.Forms.Label lb_TextPrec;
        private System.Windows.Forms.Label lb_Rec;
        private System.Windows.Forms.Label lb_Suiv;
    }
}
