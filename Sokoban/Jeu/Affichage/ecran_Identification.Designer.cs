﻿namespace Sokoban.Jeu.Affichage
{
    partial class ecran_Identification
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_Retour = new System.Windows.Forms.Button();
            this.Lsb_Comptes = new System.Windows.Forms.ListBox();
            this.lbl_Comptes = new System.Windows.Forms.Label();
            this.lbl_Compte = new System.Windows.Forms.Label();
            this.bt_Activer = new System.Windows.Forms.Button();
            this.bt_Supprimer = new System.Windows.Forms.Button();
            this.bt_Creer = new System.Windows.Forms.Button();
            this.txb_Creer = new System.Windows.Forms.TextBox();
            this.lbl_Actif = new System.Windows.Forms.Label();
            this.lbl_Actif_NomJoueur = new System.Windows.Forms.Label();
            this.bt_Joueur = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bt_Retour
            // 
            this.bt_Retour.Location = new System.Drawing.Point(99, 321);
            this.bt_Retour.Name = "bt_Retour";
            this.bt_Retour.Size = new System.Drawing.Size(105, 23);
            this.bt_Retour.TabIndex = 0;
            this.bt_Retour.Text = "Menu Principal";
            this.bt_Retour.UseVisualStyleBackColor = true;
            this.bt_Retour.Click += new System.EventHandler(this.bt_Retour_Click);
            // 
            // Lsb_Comptes
            // 
            this.Lsb_Comptes.FormattingEnabled = true;
            this.Lsb_Comptes.Location = new System.Drawing.Point(3, 17);
            this.Lsb_Comptes.Name = "Lsb_Comptes";
            this.Lsb_Comptes.ScrollAlwaysVisible = true;
            this.Lsb_Comptes.Size = new System.Drawing.Size(201, 121);
            this.Lsb_Comptes.Sorted = true;
            this.Lsb_Comptes.TabIndex = 1;
            this.Lsb_Comptes.SelectedIndexChanged += new System.EventHandler(this.Lsb_Comptes_SelectedIndexChanged);
            // 
            // lbl_Comptes
            // 
            this.lbl_Comptes.AutoSize = true;
            this.lbl_Comptes.Location = new System.Drawing.Point(3, 1);
            this.lbl_Comptes.Name = "lbl_Comptes";
            this.lbl_Comptes.Size = new System.Drawing.Size(83, 13);
            this.lbl_Comptes.TabIndex = 2;
            this.lbl_Comptes.Text = "Comptes créés :";
            // 
            // lbl_Compte
            // 
            this.lbl_Compte.AutoSize = true;
            this.lbl_Compte.Location = new System.Drawing.Point(3, 209);
            this.lbl_Compte.Name = "lbl_Compte";
            this.lbl_Compte.Size = new System.Drawing.Size(92, 13);
            this.lbl_Compte.TabIndex = 3;
            this.lbl_Compte.Text = "Créer un Compte :";
            // 
            // bt_Activer
            // 
            this.bt_Activer.Enabled = false;
            this.bt_Activer.Location = new System.Drawing.Point(210, 18);
            this.bt_Activer.Name = "bt_Activer";
            this.bt_Activer.Size = new System.Drawing.Size(75, 23);
            this.bt_Activer.TabIndex = 4;
            this.bt_Activer.Text = "Activer";
            this.bt_Activer.UseVisualStyleBackColor = true;
            this.bt_Activer.Click += new System.EventHandler(this.bt_Activer_Click);
            // 
            // bt_Supprimer
            // 
            this.bt_Supprimer.Enabled = false;
            this.bt_Supprimer.Location = new System.Drawing.Point(211, 47);
            this.bt_Supprimer.Name = "bt_Supprimer";
            this.bt_Supprimer.Size = new System.Drawing.Size(75, 23);
            this.bt_Supprimer.TabIndex = 5;
            this.bt_Supprimer.Text = "Supprimer";
            this.bt_Supprimer.UseVisualStyleBackColor = true;
            this.bt_Supprimer.Click += new System.EventHandler(this.bt_Supprimer_Click);
            // 
            // bt_Creer
            // 
            this.bt_Creer.Location = new System.Drawing.Point(211, 223);
            this.bt_Creer.Name = "bt_Creer";
            this.bt_Creer.Size = new System.Drawing.Size(75, 23);
            this.bt_Creer.TabIndex = 6;
            this.bt_Creer.Text = "Créer";
            this.bt_Creer.UseVisualStyleBackColor = true;
            this.bt_Creer.Click += new System.EventHandler(this.bt_Creer_Click);
            // 
            // txb_Creer
            // 
            this.txb_Creer.Location = new System.Drawing.Point(6, 225);
            this.txb_Creer.Name = "txb_Creer";
            this.txb_Creer.Size = new System.Drawing.Size(198, 20);
            this.txb_Creer.TabIndex = 7;
            this.txb_Creer.TextChanged += new System.EventHandler(this.txb_Creer_TextChanged);
            // 
            // lbl_Actif
            // 
            this.lbl_Actif.AutoSize = true;
            this.lbl_Actif.Location = new System.Drawing.Point(3, 141);
            this.lbl_Actif.Name = "lbl_Actif";
            this.lbl_Actif.Size = new System.Drawing.Size(141, 13);
            this.lbl_Actif.TabIndex = 3;
            this.lbl_Actif.Text = "Compte actuellement utilisé :";
            // 
            // lbl_Actif_NomJoueur
            // 
            this.lbl_Actif_NomJoueur.AutoSize = true;
            this.lbl_Actif_NomJoueur.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Actif_NomJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Actif_NomJoueur.Location = new System.Drawing.Point(14, 164);
            this.lbl_Actif_NomJoueur.Name = "lbl_Actif_NomJoueur";
            this.lbl_Actif_NomJoueur.Size = new System.Drawing.Size(72, 13);
            this.lbl_Actif_NomJoueur.TabIndex = 8;
            this.lbl_Actif_NomJoueur.Text = "Personne...";
            // 
            // bt_Joueur
            // 
            this.bt_Joueur.Location = new System.Drawing.Point(210, 154);
            this.bt_Joueur.Name = "bt_Joueur";
            this.bt_Joueur.Size = new System.Drawing.Size(76, 23);
            this.bt_Joueur.TabIndex = 0;
            this.bt_Joueur.Text = "Jouer";
            this.bt_Joueur.UseVisualStyleBackColor = true;
            this.bt_Joueur.Visible = false;
            this.bt_Joueur.Click += new System.EventHandler(this.bt_Jouer_Click);
            // 
            // ecran_Identification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lbl_Actif_NomJoueur);
            this.Controls.Add(this.txb_Creer);
            this.Controls.Add(this.bt_Creer);
            this.Controls.Add(this.bt_Supprimer);
            this.Controls.Add(this.bt_Activer);
            this.Controls.Add(this.lbl_Actif);
            this.Controls.Add(this.lbl_Compte);
            this.Controls.Add(this.lbl_Comptes);
            this.Controls.Add(this.Lsb_Comptes);
            this.Controls.Add(this.bt_Joueur);
            this.Controls.Add(this.bt_Retour);
            this.Name = "ecran_Identification";
            this.Size = new System.Drawing.Size(297, 359);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_Retour;
        private System.Windows.Forms.ListBox Lsb_Comptes;
        private System.Windows.Forms.Label lbl_Comptes;
        private System.Windows.Forms.Label lbl_Compte;
        private System.Windows.Forms.Button bt_Activer;
        private System.Windows.Forms.Button bt_Supprimer;
        private System.Windows.Forms.Button bt_Creer;
        private System.Windows.Forms.TextBox txb_Creer;
        private System.Windows.Forms.Label lbl_Actif;
        private System.Windows.Forms.Label lbl_Actif_NomJoueur;
        private System.Windows.Forms.Button bt_Joueur;
    }
}
