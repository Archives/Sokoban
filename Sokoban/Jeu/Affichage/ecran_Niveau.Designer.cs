﻿namespace Sokoban.Jeu.Affichage
{
    partial class ecran_Niveau
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.jeu_Niveau = new System.Windows.Forms.Panel();
            this.lbl_TempsTitre = new System.Windows.Forms.Label();
            this.lbl_DeplacementTitre = new System.Windows.Forms.Label();
            this.lbl_Temps = new System.Windows.Forms.Label();
            this.lbl_Deplacement = new System.Windows.Forms.Label();
            this.lbl_Nom = new System.Windows.Forms.Label();
            this.bt_MenuPrincipal = new System.Windows.Forms.Button();
            this.bt_Undo = new System.Windows.Forms.Button();
            this.bt_Recommencer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // jeu_Niveau
            // 
            this.jeu_Niveau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jeu_Niveau.Location = new System.Drawing.Point(0, 0);
            this.jeu_Niveau.Name = "jeu_Niveau";
            this.jeu_Niveau.Size = new System.Drawing.Size(655, 397);
            this.jeu_Niveau.TabIndex = 0;
            // 
            // lbl_TempsTitre
            // 
            this.lbl_TempsTitre.AutoSize = true;
            this.lbl_TempsTitre.Location = new System.Drawing.Point(2, 375);
            this.lbl_TempsTitre.Name = "lbl_TempsTitre";
            this.lbl_TempsTitre.Size = new System.Drawing.Size(39, 13);
            this.lbl_TempsTitre.TabIndex = 1;
            this.lbl_TempsTitre.Text = "Temps";
            // 
            // lbl_DeplacementTitre
            // 
            this.lbl_DeplacementTitre.AutoSize = true;
            this.lbl_DeplacementTitre.Location = new System.Drawing.Point(115, 375);
            this.lbl_DeplacementTitre.Name = "lbl_DeplacementTitre";
            this.lbl_DeplacementTitre.Size = new System.Drawing.Size(70, 13);
            this.lbl_DeplacementTitre.TabIndex = 1;
            this.lbl_DeplacementTitre.Text = "Déplacement";
            // 
            // lbl_Temps
            // 
            this.lbl_Temps.AutoSize = true;
            this.lbl_Temps.Location = new System.Drawing.Point(47, 375);
            this.lbl_Temps.Name = "lbl_Temps";
            this.lbl_Temps.Size = new System.Drawing.Size(34, 13);
            this.lbl_Temps.TabIndex = 1;
            this.lbl_Temps.Text = "00:00";
            // 
            // lbl_Deplacement
            // 
            this.lbl_Deplacement.AutoSize = true;
            this.lbl_Deplacement.Location = new System.Drawing.Point(191, 375);
            this.lbl_Deplacement.Name = "lbl_Deplacement";
            this.lbl_Deplacement.Size = new System.Drawing.Size(31, 13);
            this.lbl_Deplacement.TabIndex = 1;
            this.lbl_Deplacement.Text = "0000";
            // 
            // lbl_Nom
            // 
            this.lbl_Nom.AutoSize = true;
            this.lbl_Nom.Location = new System.Drawing.Point(258, 375);
            this.lbl_Nom.Name = "lbl_Nom";
            this.lbl_Nom.Size = new System.Drawing.Size(81, 13);
            this.lbl_Nom.TabIndex = 1;
            this.lbl_Nom.Text = "Nom du Niveau";
            // 
            // bt_MenuPrincipal
            // 
            this.bt_MenuPrincipal.Location = new System.Drawing.Point(564, 365);
            this.bt_MenuPrincipal.Name = "bt_MenuPrincipal";
            this.bt_MenuPrincipal.Size = new System.Drawing.Size(86, 23);
            this.bt_MenuPrincipal.TabIndex = 2;
            this.bt_MenuPrincipal.Text = "Menu Principal";
            this.bt_MenuPrincipal.UseVisualStyleBackColor = true;
            this.bt_MenuPrincipal.Click += new System.EventHandler(this.bt_MenuPrincipal_Click);
            // 
            // bt_Undo
            // 
            this.bt_Undo.Location = new System.Drawing.Point(516, 365);
            this.bt_Undo.Name = "bt_Undo";
            this.bt_Undo.Size = new System.Drawing.Size(42, 23);
            this.bt_Undo.TabIndex = 2;
            this.bt_Undo.Text = "Undo";
            this.bt_Undo.UseVisualStyleBackColor = true;
            this.bt_Undo.Click += new System.EventHandler(this.bt_Undo_Click);
            // 
            // bt_Recommencer
            // 
            this.bt_Recommencer.Location = new System.Drawing.Point(424, 365);
            this.bt_Recommencer.Name = "bt_Recommencer";
            this.bt_Recommencer.Size = new System.Drawing.Size(86, 23);
            this.bt_Recommencer.TabIndex = 2;
            this.bt_Recommencer.Text = "Recommencer";
            this.bt_Recommencer.UseVisualStyleBackColor = true;
            this.bt_Recommencer.Click += new System.EventHandler(this.bt_Recommencer_Click);
            // 
            // ecran_Niveau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bt_Recommencer);
            this.Controls.Add(this.bt_Undo);
            this.Controls.Add(this.bt_MenuPrincipal);
            this.Controls.Add(this.lbl_Deplacement);
            this.Controls.Add(this.lbl_Nom);
            this.Controls.Add(this.lbl_Temps);
            this.Controls.Add(this.lbl_DeplacementTitre);
            this.Controls.Add(this.lbl_TempsTitre);
            this.Controls.Add(this.jeu_Niveau);
            this.Name = "ecran_Niveau";
            this.Size = new System.Drawing.Size(655, 397);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel jeu_Niveau;
        private System.Windows.Forms.Label lbl_TempsTitre;
        private System.Windows.Forms.Label lbl_DeplacementTitre;
        private System.Windows.Forms.Label lbl_Temps;
        private System.Windows.Forms.Label lbl_Deplacement;
        private System.Windows.Forms.Label lbl_Nom;
        private System.Windows.Forms.Button bt_MenuPrincipal;
        private System.Windows.Forms.Button bt_Undo;
        private System.Windows.Forms.Button bt_Recommencer;
    }
}
