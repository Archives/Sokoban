using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Sokoban.Jeu;


namespace Sokoban.Jeu.Affichage
{
    public partial class ecran_MenuPrincipal : UserControl
    {
        private JeuSokoban jeuSokoban;
        
        public ecran_MenuPrincipal(JeuSokoban jeuSokoban)
        {
            this.jeuSokoban = jeuSokoban;

            InitializeComponent();

            if (jeuSokoban.siJoueur())
            {
                
            }
            else
            {
                
            }

        }

        private void bt_NouvellePartie_Click(object sender, EventArgs e)
        {
            jeuSokoban.nouvellePartieToolStripMenuItem_Click(sender, e);
        }

        private void bt_CompteJoueur_Click(object sender, EventArgs e)
        {
            jeuSokoban.sidentifierToolStripMenuItem_Click(sender, e);
        }

        private void bt_Quitter_Click(object sender, EventArgs e)
        {
            jeuSokoban.quitterToolStripMenuItem_Click(sender, e);
        }

     


        

    }
}
