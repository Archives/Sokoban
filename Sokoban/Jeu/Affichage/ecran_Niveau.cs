using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Sokoban.Jeu.Action;
using Sokoban.Jeu.Niveau;

namespace Sokoban.Jeu.Affichage
{
    public partial class ecran_Niveau : UserControl
    {
        
        private JeuSokoban jeuSokoban;
        private ecran_Jeu niv; 

        public ecran_Niveau(JeuSokoban jeuSokoban, NivInfos niveau)
        {
            this.jeuSokoban = jeuSokoban;
            InitializeComponent();
            jeu_Niveau.Controls.Clear();
            niv = new ecran_Jeu(this, niveau);
            jeu_Niveau.Controls.Add(niv);
            lbl_Nom.Text = niveau.Nom;
        }

        public void mouvementChange(int value) 
        {
            if (Clock == null) 
            {
                startMinute = DateTime.Now.Minute;
                startSeconde = DateTime.Now.Second;
                startMilli = DateTime.Now.Millisecond;
                timer();
            }
            lbl_Deplacement.Text = String.Format("{0:D4}", value);
        }

        private int startMinute = 0;
        private int startSeconde = 0;
        private int startMilli = 0;
        private int courantMinute = 0;
        private int courantSeconde = 0;
        private int courantMilli = 0;
        
        private Timer Clock;
        private void timer()
        {
            Clock = new Timer();
            Clock.Interval=1000;
            Clock.Start();
            Clock.Tick += new EventHandler(Timer_Tick);
            lbl_Temps.Text = getTime();
        }

        public string getTime()
        {
            string TimeInString;
           
            int courant = duree();

            courantMinute = courant / 60000;
            courantSeconde = (courant % 60000) / 1000;
            courantMilli = courant % 60000;

            TimeInString = String.Format("{0:D2}", courantMinute); 
            TimeInString += ":" +  String.Format("{0:D2}", courantSeconde); 
            return TimeInString;
        }

        public void Timer_Tick(object sender, EventArgs eArgs)
        {
            if (sender == Clock)
            {
                lbl_Temps.Text = getTime();
            }
        }

        private int duree () {
            int start = startMinute*60000+startSeconde*1000+startMilli;
            
            int minute = DateTime.Now.Minute;
            if (startMinute > minute)
            {
                minute += 60;
            }
            
            int courant = minute*60000+DateTime.Now.Second*1000+DateTime.Now.Millisecond;

            courant -=  start;
            return courant;
        }

        public void fin(int mouvement)
        {
            jeuSokoban.NiveauInfos.setScore(duree(), mouvement);
            jeuSokoban.ecran(JeuSokoban.NIVEAUFINI);
        }

        private void bt_Recommencer_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.activerNiveau(jeuSokoban.NiveauInfos.jeuNom, jeuSokoban.NiveauInfos.jeuLigne, jeuSokoban.NiveauInfos.jeuNiveau);
            jeuSokoban.ecran(JeuSokoban.NIVEAU);
        }

        private void bt_Undo_Click(object sender, EventArgs e)
        {
            niv.undo();
        }

        private void bt_MenuPrincipal_Click(object sender, EventArgs e)
        {
            jeuSokoban.EcranSuivant = JeuSokoban.VIDE;
            jeuSokoban.ecran(JeuSokoban.MENU_PRINCIPAL);
        }


    }
}
