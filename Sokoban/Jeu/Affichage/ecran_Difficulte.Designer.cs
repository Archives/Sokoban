﻿namespace Sokoban.Jeu.Affichage
{
    partial class ecran_Difficulte
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_lv1 = new System.Windows.Forms.Button();
            this.bt_lv2 = new System.Windows.Forms.Button();
            this.bt_Retour = new System.Windows.Forms.Button();
            this.bt_lvl3 = new System.Windows.Forms.Button();
            this.bt_Reprendre = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bt_lv1
            // 
            this.bt_lv1.Location = new System.Drawing.Point(145, 112);
            this.bt_lv1.Name = "bt_lv1";
            this.bt_lv1.Size = new System.Drawing.Size(111, 23);
            this.bt_lv1.TabIndex = 0;
            this.bt_lv1.Text = "Facile";
            this.bt_lv1.UseVisualStyleBackColor = true;
            this.bt_lv1.Click += new System.EventHandler(this.bt_lv1_Click);
            // 
            // bt_lv2
            // 
            this.bt_lv2.Location = new System.Drawing.Point(145, 141);
            this.bt_lv2.Name = "bt_lv2";
            this.bt_lv2.Size = new System.Drawing.Size(111, 23);
            this.bt_lv2.TabIndex = 1;
            this.bt_lv2.Text = "Moyen";
            this.bt_lv2.UseVisualStyleBackColor = true;
            this.bt_lv2.Click += new System.EventHandler(this.bt_lv2_Click);
            // 
            // bt_Retour
            // 
            this.bt_Retour.Location = new System.Drawing.Point(145, 279);
            this.bt_Retour.Name = "bt_Retour";
            this.bt_Retour.Size = new System.Drawing.Size(111, 23);
            this.bt_Retour.TabIndex = 1;
            this.bt_Retour.Text = "Menu Principal";
            this.bt_Retour.UseVisualStyleBackColor = true;
            this.bt_Retour.Click += new System.EventHandler(this.bt_Retour_Click);
            // 
            // bt_lvl3
            // 
            this.bt_lvl3.Location = new System.Drawing.Point(145, 170);
            this.bt_lvl3.Name = "bt_lvl3";
            this.bt_lvl3.Size = new System.Drawing.Size(111, 23);
            this.bt_lvl3.TabIndex = 1;
            this.bt_lvl3.Text = "Difficile";
            this.bt_lvl3.UseVisualStyleBackColor = true;
            this.bt_lvl3.Click += new System.EventHandler(this.bt_lvl3_Click);
            // 
            // bt_Reprendre
            // 
            this.bt_Reprendre.Location = new System.Drawing.Point(145, 62);
            this.bt_Reprendre.Name = "bt_Reprendre";
            this.bt_Reprendre.Size = new System.Drawing.Size(111, 23);
            this.bt_Reprendre.TabIndex = 0;
            this.bt_Reprendre.Text = "Reprendre";
            this.bt_Reprendre.UseVisualStyleBackColor = true;
            this.bt_Reprendre.Click += new System.EventHandler(this.bt_Reprendre_Click);
            // 
            // ecran_Difficulte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bt_Retour);
            this.Controls.Add(this.bt_lvl3);
            this.Controls.Add(this.bt_lv2);
            this.Controls.Add(this.bt_Reprendre);
            this.Controls.Add(this.bt_lv1);
            this.Name = "ecran_Difficulte";
            this.Size = new System.Drawing.Size(425, 337);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_lv1;
        private System.Windows.Forms.Button bt_lv2;
        private System.Windows.Forms.Button bt_Retour;
        private System.Windows.Forms.Button bt_lvl3;
        private System.Windows.Forms.Button bt_Reprendre;
    }
}
