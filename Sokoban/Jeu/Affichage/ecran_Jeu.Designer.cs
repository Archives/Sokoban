﻿namespace Sokoban.Jeu.Affichage
{
    partial class ecran_Jeu
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.personnage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.personnage)).BeginInit();
            this.SuspendLayout();
            // 
            // personnage
            // 
            this.personnage.BackColor = System.Drawing.Color.Transparent;
            this.personnage.Location = new System.Drawing.Point(274, 70);
            this.personnage.Name = "personnage";
            this.personnage.Size = new System.Drawing.Size(38, 36);
            this.personnage.TabIndex = 1;
            this.personnage.TabStop = false;
            // 
            // ecran_Jeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.personnage);
            this.Name = "ecran_Jeu";
            this.Size = new System.Drawing.Size(851, 552);
            ((System.ComponentModel.ISupportInitialize)(this.personnage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox personnage;
    }
}
