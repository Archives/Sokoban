﻿namespace Sokoban.Jeu.Affichage
{
    partial class ecran_CarteNiveaux
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.grilleNiveaux = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_DifficulteTitre = new System.Windows.Forms.Label();
            this.lbl_NumeroTitre = new System.Windows.Forms.Label();
            this.lbl_NbrCaissesTitre = new System.Windows.Forms.Label();
            this.lbl_EtatNiveau = new System.Windows.Forms.Label();
            this.lbl_NbrCaisses = new System.Windows.Forms.Label();
            this.lbl_Numero = new System.Windows.Forms.Label();
            this.lbl_Difficulte = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_Joueur_Temps = new System.Windows.Forms.Label();
            this.lbl_Joueur_TempsTitre = new System.Windows.Forms.Label();
            this.lbl_Joueur_Deplacement = new System.Windows.Forms.Label();
            this.lbl_Joueur_Nom = new System.Windows.Forms.Label();
            this.lbl_Joueur_DeplacementTitre = new System.Windows.Forms.Label();
            this.lbl_Meilleur_DeplacementTitrre = new System.Windows.Forms.Label();
            this.lbl_Meilleur_Nom = new System.Windows.Forms.Label();
            this.lbl_Meilleur_Deplacement = new System.Windows.Forms.Label();
            this.lbl_Meilleur_TempsTitre = new System.Windows.Forms.Label();
            this.lbl_Meilleur_Temps = new System.Windows.Forms.Label();
            this.bt_LancerPartie = new System.Windows.Forms.Button();
            this.bt_MenuPrincipal = new System.Windows.Forms.Button();
            this.miniNiveau = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grilleNiveaux
            // 
            this.grilleNiveaux.ColumnCount = 10;
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.Location = new System.Drawing.Point(0, 3);
            this.grilleNiveaux.Name = "grilleNiveaux";
            this.grilleNiveaux.RowCount = 10;
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.grilleNiveaux.Size = new System.Drawing.Size(542, 469);
            this.grilleNiveaux.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_EtatNiveau);
            this.groupBox1.Controls.Add(this.lbl_Difficulte);
            this.groupBox1.Controls.Add(this.lbl_Numero);
            this.groupBox1.Controls.Add(this.lbl_DifficulteTitre);
            this.groupBox1.Controls.Add(this.lbl_NbrCaisses);
            this.groupBox1.Controls.Add(this.lbl_NumeroTitre);
            this.groupBox1.Controls.Add(this.lbl_NbrCaissesTitre);
            this.groupBox1.Location = new System.Drawing.Point(548, 144);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(177, 135);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nom niveau";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lbl_DifficulteTitre
            // 
            this.lbl_DifficulteTitre.AutoSize = true;
            this.lbl_DifficulteTitre.Location = new System.Drawing.Point(6, 36);
            this.lbl_DifficulteTitre.Name = "lbl_DifficulteTitre";
            this.lbl_DifficulteTitre.Size = new System.Drawing.Size(48, 13);
            this.lbl_DifficulteTitre.TabIndex = 0;
            this.lbl_DifficulteTitre.Text = "Difficulté";
            // 
            // lbl_NumeroTitre
            // 
            this.lbl_NumeroTitre.AutoSize = true;
            this.lbl_NumeroTitre.Location = new System.Drawing.Point(6, 17);
            this.lbl_NumeroTitre.Name = "lbl_NumeroTitre";
            this.lbl_NumeroTitre.Size = new System.Drawing.Size(44, 13);
            this.lbl_NumeroTitre.TabIndex = 1;
            this.lbl_NumeroTitre.Text = "Numero";
            // 
            // lbl_NbrCaissesTitre
            // 
            this.lbl_NbrCaissesTitre.AutoSize = true;
            this.lbl_NbrCaissesTitre.Location = new System.Drawing.Point(6, 55);
            this.lbl_NbrCaissesTitre.Name = "lbl_NbrCaissesTitre";
            this.lbl_NbrCaissesTitre.Size = new System.Drawing.Size(39, 13);
            this.lbl_NbrCaissesTitre.TabIndex = 2;
            this.lbl_NbrCaissesTitre.Text = "Blocks";
            this.lbl_NbrCaissesTitre.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbl_EtatNiveau
            // 
            this.lbl_EtatNiveau.AutoSize = true;
            this.lbl_EtatNiveau.Location = new System.Drawing.Point(6, 74);
            this.lbl_EtatNiveau.Name = "lbl_EtatNiveau";
            this.lbl_EtatNiveau.Size = new System.Drawing.Size(52, 13);
            this.lbl_EtatNiveau.TabIndex = 12;
            this.lbl_EtatNiveau.Text = "Inachevé";
            this.lbl_EtatNiveau.Click += new System.EventHandler(this.label4_Click);
            // 
            // lbl_NbrCaisses
            // 
            this.lbl_NbrCaisses.AutoSize = true;
            this.lbl_NbrCaisses.Location = new System.Drawing.Point(89, 55);
            this.lbl_NbrCaisses.Name = "lbl_NbrCaisses";
            this.lbl_NbrCaisses.Size = new System.Drawing.Size(25, 13);
            this.lbl_NbrCaisses.TabIndex = 2;
            this.lbl_NbrCaisses.Text = "000";
            this.lbl_NbrCaisses.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbl_Numero
            // 
            this.lbl_Numero.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.lbl_Numero.AutoSize = true;
            this.lbl_Numero.Location = new System.Drawing.Point(89, 17);
            this.lbl_Numero.Name = "lbl_Numero";
            this.lbl_Numero.Size = new System.Drawing.Size(28, 13);
            this.lbl_Numero.TabIndex = 1;
            this.lbl_Numero.Text = "1 - 1";
            // 
            // lbl_Difficulte
            // 
            this.lbl_Difficulte.AutoSize = true;
            this.lbl_Difficulte.Location = new System.Drawing.Point(89, 36);
            this.lbl_Difficulte.Name = "lbl_Difficulte";
            this.lbl_Difficulte.Size = new System.Drawing.Size(38, 13);
            this.lbl_Difficulte.TabIndex = 0;
            this.lbl_Difficulte.Text = "Facile ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_Meilleur_Temps);
            this.groupBox2.Controls.Add(this.lbl_Joueur_Temps);
            this.groupBox2.Controls.Add(this.lbl_Meilleur_TempsTitre);
            this.groupBox2.Controls.Add(this.lbl_Joueur_TempsTitre);
            this.groupBox2.Controls.Add(this.lbl_Meilleur_Deplacement);
            this.groupBox2.Controls.Add(this.lbl_Joueur_Deplacement);
            this.groupBox2.Controls.Add(this.lbl_Meilleur_Nom);
            this.groupBox2.Controls.Add(this.lbl_Meilleur_DeplacementTitrre);
            this.groupBox2.Controls.Add(this.lbl_Joueur_Nom);
            this.groupBox2.Controls.Add(this.lbl_Joueur_DeplacementTitre);
            this.groupBox2.Location = new System.Drawing.Point(548, 285);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(177, 140);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Meilleurs Scores";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lbl_Joueur_Temps
            // 
            this.lbl_Joueur_Temps.AutoSize = true;
            this.lbl_Joueur_Temps.Location = new System.Drawing.Point(89, 36);
            this.lbl_Joueur_Temps.Name = "lbl_Joueur_Temps";
            this.lbl_Joueur_Temps.Size = new System.Drawing.Size(36, 13);
            this.lbl_Joueur_Temps.TabIndex = 0;
            this.lbl_Joueur_Temps.Text = "xxx sc";
            // 
            // lbl_Joueur_TempsTitre
            // 
            this.lbl_Joueur_TempsTitre.AutoSize = true;
            this.lbl_Joueur_TempsTitre.Location = new System.Drawing.Point(6, 36);
            this.lbl_Joueur_TempsTitre.Name = "lbl_Joueur_TempsTitre";
            this.lbl_Joueur_TempsTitre.Size = new System.Drawing.Size(39, 13);
            this.lbl_Joueur_TempsTitre.TabIndex = 0;
            this.lbl_Joueur_TempsTitre.Text = "Temps";
            // 
            // lbl_Joueur_Deplacement
            // 
            this.lbl_Joueur_Deplacement.AutoSize = true;
            this.lbl_Joueur_Deplacement.Location = new System.Drawing.Point(89, 55);
            this.lbl_Joueur_Deplacement.Name = "lbl_Joueur_Deplacement";
            this.lbl_Joueur_Deplacement.Size = new System.Drawing.Size(25, 13);
            this.lbl_Joueur_Deplacement.TabIndex = 2;
            this.lbl_Joueur_Deplacement.Text = "000";
            this.lbl_Joueur_Deplacement.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbl_Joueur_Nom
            // 
            this.lbl_Joueur_Nom.AutoSize = true;
            this.lbl_Joueur_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Joueur_Nom.Location = new System.Drawing.Point(6, 17);
            this.lbl_Joueur_Nom.Name = "lbl_Joueur_Nom";
            this.lbl_Joueur_Nom.Size = new System.Drawing.Size(92, 13);
            this.lbl_Joueur_Nom.TabIndex = 1;
            this.lbl_Joueur_Nom.Text = "Nom du Joueur";
            // 
            // lbl_Joueur_DeplacementTitre
            // 
            this.lbl_Joueur_DeplacementTitre.AutoSize = true;
            this.lbl_Joueur_DeplacementTitre.Location = new System.Drawing.Point(6, 55);
            this.lbl_Joueur_DeplacementTitre.Name = "lbl_Joueur_DeplacementTitre";
            this.lbl_Joueur_DeplacementTitre.Size = new System.Drawing.Size(63, 13);
            this.lbl_Joueur_DeplacementTitre.TabIndex = 2;
            this.lbl_Joueur_DeplacementTitre.Text = "Mouvement";
            this.lbl_Joueur_DeplacementTitre.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbl_Meilleur_DeplacementTitrre
            // 
            this.lbl_Meilleur_DeplacementTitrre.AutoSize = true;
            this.lbl_Meilleur_DeplacementTitrre.Location = new System.Drawing.Point(6, 118);
            this.lbl_Meilleur_DeplacementTitrre.Name = "lbl_Meilleur_DeplacementTitrre";
            this.lbl_Meilleur_DeplacementTitrre.Size = new System.Drawing.Size(63, 13);
            this.lbl_Meilleur_DeplacementTitrre.TabIndex = 2;
            this.lbl_Meilleur_DeplacementTitrre.Text = "Mouvement";
            this.lbl_Meilleur_DeplacementTitrre.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbl_Meilleur_Nom
            // 
            this.lbl_Meilleur_Nom.AutoSize = true;
            this.lbl_Meilleur_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Meilleur_Nom.Location = new System.Drawing.Point(6, 80);
            this.lbl_Meilleur_Nom.Name = "lbl_Meilleur_Nom";
            this.lbl_Meilleur_Nom.Size = new System.Drawing.Size(98, 13);
            this.lbl_Meilleur_Nom.TabIndex = 1;
            this.lbl_Meilleur_Nom.Text = "Nom du Meilleur";
            // 
            // lbl_Meilleur_Deplacement
            // 
            this.lbl_Meilleur_Deplacement.AutoSize = true;
            this.lbl_Meilleur_Deplacement.Location = new System.Drawing.Point(89, 118);
            this.lbl_Meilleur_Deplacement.Name = "lbl_Meilleur_Deplacement";
            this.lbl_Meilleur_Deplacement.Size = new System.Drawing.Size(25, 13);
            this.lbl_Meilleur_Deplacement.TabIndex = 2;
            this.lbl_Meilleur_Deplacement.Text = "000";
            this.lbl_Meilleur_Deplacement.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbl_Meilleur_TempsTitre
            // 
            this.lbl_Meilleur_TempsTitre.AutoSize = true;
            this.lbl_Meilleur_TempsTitre.Location = new System.Drawing.Point(6, 99);
            this.lbl_Meilleur_TempsTitre.Name = "lbl_Meilleur_TempsTitre";
            this.lbl_Meilleur_TempsTitre.Size = new System.Drawing.Size(39, 13);
            this.lbl_Meilleur_TempsTitre.TabIndex = 0;
            this.lbl_Meilleur_TempsTitre.Text = "Temps";
            // 
            // lbl_Meilleur_Temps
            // 
            this.lbl_Meilleur_Temps.AutoSize = true;
            this.lbl_Meilleur_Temps.Location = new System.Drawing.Point(89, 99);
            this.lbl_Meilleur_Temps.Name = "lbl_Meilleur_Temps";
            this.lbl_Meilleur_Temps.Size = new System.Drawing.Size(36, 13);
            this.lbl_Meilleur_Temps.TabIndex = 0;
            this.lbl_Meilleur_Temps.Text = "xxx sc";
            // 
            // bt_LancerPartie
            // 
            this.bt_LancerPartie.Location = new System.Drawing.Point(548, 115);
            this.bt_LancerPartie.Name = "bt_LancerPartie";
            this.bt_LancerPartie.Size = new System.Drawing.Size(177, 23);
            this.bt_LancerPartie.TabIndex = 12;
            this.bt_LancerPartie.Text = "Lancer la Partie";
            this.bt_LancerPartie.UseVisualStyleBackColor = true;
            // 
            // bt_MenuPrincipal
            // 
            this.bt_MenuPrincipal.Location = new System.Drawing.Point(548, 431);
            this.bt_MenuPrincipal.Name = "bt_MenuPrincipal";
            this.bt_MenuPrincipal.Size = new System.Drawing.Size(177, 23);
            this.bt_MenuPrincipal.TabIndex = 12;
            this.bt_MenuPrincipal.Text = "Menu Principal";
            this.bt_MenuPrincipal.UseVisualStyleBackColor = true;
            // 
            // miniNiveau
            // 
            this.miniNiveau.Location = new System.Drawing.Point(548, 9);
            this.miniNiveau.Name = "miniNiveau";
            this.miniNiveau.Size = new System.Drawing.Size(177, 100);
            this.miniNiveau.TabIndex = 13;
            // 
            // ecran_CarteNiveaux
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.miniNiveau);
            this.Controls.Add(this.bt_MenuPrincipal);
            this.Controls.Add(this.bt_LancerPartie);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grilleNiveaux);
            this.Name = "ecran_CarteNiveaux";
            this.Size = new System.Drawing.Size(728, 472);
            this.Load += new System.EventHandler(this.CarteNiveaux_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel grilleNiveaux;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_NumeroTitre;
        private System.Windows.Forms.Label lbl_DifficulteTitre;
        private System.Windows.Forms.Label lbl_NbrCaissesTitre;
        private System.Windows.Forms.Label lbl_EtatNiveau;
        private System.Windows.Forms.Label lbl_Difficulte;
        private System.Windows.Forms.Label lbl_Numero;
        private System.Windows.Forms.Label lbl_NbrCaisses;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_Joueur_Temps;
        private System.Windows.Forms.Label lbl_Joueur_TempsTitre;
        private System.Windows.Forms.Label lbl_Joueur_Deplacement;
        private System.Windows.Forms.Label lbl_Joueur_Nom;
        private System.Windows.Forms.Label lbl_Joueur_DeplacementTitre;
        private System.Windows.Forms.Label lbl_Meilleur_Temps;
        private System.Windows.Forms.Label lbl_Meilleur_TempsTitre;
        private System.Windows.Forms.Label lbl_Meilleur_Deplacement;
        private System.Windows.Forms.Label lbl_Meilleur_Nom;
        private System.Windows.Forms.Label lbl_Meilleur_DeplacementTitrre;
        private System.Windows.Forms.Button bt_LancerPartie;
        private System.Windows.Forms.Button bt_MenuPrincipal;
        private System.Windows.Forms.Panel miniNiveau;
    }
}
