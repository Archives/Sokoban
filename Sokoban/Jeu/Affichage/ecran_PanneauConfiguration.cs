using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Sokoban.Jeu.Affichage
{
    public partial class ecran_PanneauConfiguration : UserControl
    {
        JeuSokoban jeuSokoban;
        
        public ecran_PanneauConfiguration(JeuSokoban jeuSokoban)
        {
            this.jeuSokoban = jeuSokoban;
            InitializeComponent();
            cb_Fenetre.Checked = jeuSokoban.Configuration.saveDimensions;
            cb_Joueur.Checked = jeuSokoban.Configuration.saveDernierJoueur;
        }

        private void cb_Fenetre_CheckedChanged(object sender, EventArgs e)
        {
            jeuSokoban.Configuration.saveDimensions = cb_Fenetre.Checked;
        }

        private void cb_Joueur_CheckedChanged(object sender, EventArgs e)
        {
            jeuSokoban.Configuration.saveDernierJoueur = cb_Joueur.Checked;
        }
    }
}
