﻿namespace Sokoban.Jeu.Affichage
{
    partial class ecran_MenuPrincipal
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_NouvellePartie = new System.Windows.Forms.Button();
            this.bt_CompteJoueur = new System.Windows.Forms.Button();
            this.bt_Quitter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bt_NouvellePartie
            // 
            this.bt_NouvellePartie.Location = new System.Drawing.Point(94, 90);
            this.bt_NouvellePartie.Name = "bt_NouvellePartie";
            this.bt_NouvellePartie.Size = new System.Drawing.Size(177, 23);
            this.bt_NouvellePartie.TabIndex = 0;
            this.bt_NouvellePartie.Text = "Nouvelle Partie";
            this.bt_NouvellePartie.UseVisualStyleBackColor = true;
            this.bt_NouvellePartie.Click += new System.EventHandler(this.bt_NouvellePartie_Click);
            // 
            // bt_CompteJoueur
            // 
            this.bt_CompteJoueur.Location = new System.Drawing.Point(94, 119);
            this.bt_CompteJoueur.Name = "bt_CompteJoueur";
            this.bt_CompteJoueur.Size = new System.Drawing.Size(177, 23);
            this.bt_CompteJoueur.TabIndex = 0;
            this.bt_CompteJoueur.Text = "Identification";
            this.bt_CompteJoueur.UseVisualStyleBackColor = true;
            this.bt_CompteJoueur.Click += new System.EventHandler(this.bt_CompteJoueur_Click);
            // 
            // bt_Quitter
            // 
            this.bt_Quitter.Location = new System.Drawing.Point(94, 268);
            this.bt_Quitter.Name = "bt_Quitter";
            this.bt_Quitter.Size = new System.Drawing.Size(177, 23);
            this.bt_Quitter.TabIndex = 0;
            this.bt_Quitter.Text = "Quitter";
            this.bt_Quitter.UseVisualStyleBackColor = true;
            this.bt_Quitter.Click += new System.EventHandler(this.bt_Quitter_Click);
            // 
            // ecran_MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bt_Quitter);
            this.Controls.Add(this.bt_CompteJoueur);
            this.Controls.Add(this.bt_NouvellePartie);
            this.Name = "ecran_MenuPrincipal";
            this.Size = new System.Drawing.Size(375, 329);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_NouvellePartie;
        private System.Windows.Forms.Button bt_CompteJoueur;
        private System.Windows.Forms.Button bt_Quitter;

    }
}
