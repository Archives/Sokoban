﻿namespace Sokoban.Jeu.Affichage
{

    partial class ecran_PanneauConfiguration
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_Fenetre = new System.Windows.Forms.CheckBox();
            this.cb_Joueur = new System.Windows.Forms.CheckBox();
            this.lb_titre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cb_Fenetre
            // 
            this.cb_Fenetre.AutoSize = true;
            this.cb_Fenetre.Location = new System.Drawing.Point(24, 61);
            this.cb_Fenetre.Name = "cb_Fenetre";
            this.cb_Fenetre.Size = new System.Drawing.Size(278, 17);
            this.cb_Fenetre.TabIndex = 0;
            this.cb_Fenetre.Text = "Position de la fenêtre (Position absolue et dimensions)";
            this.cb_Fenetre.UseVisualStyleBackColor = true;
            this.cb_Fenetre.CheckedChanged += new System.EventHandler(this.cb_Fenetre_CheckedChanged);
            // 
            // cb_Joueur
            // 
            this.cb_Joueur.AutoSize = true;
            this.cb_Joueur.Location = new System.Drawing.Point(24, 100);
            this.cb_Joueur.Name = "cb_Joueur";
            this.cb_Joueur.Size = new System.Drawing.Size(318, 17);
            this.cb_Joueur.TabIndex = 1;
            this.cb_Joueur.Text = "Lancer automatiquement le dernier joueur au démarage du jeu";
            this.cb_Joueur.UseVisualStyleBackColor = true;
            this.cb_Joueur.CheckedChanged += new System.EventHandler(this.cb_Joueur_CheckedChanged);
            // 
            // lb_titre
            // 
            this.lb_titre.AutoSize = true;
            this.lb_titre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_titre.Location = new System.Drawing.Point(21, 19);
            this.lb_titre.Name = "lb_titre";
            this.lb_titre.Size = new System.Drawing.Size(199, 17);
            this.lb_titre.TabIndex = 2;
            this.lb_titre.Text = "Options de Configuration :";
            // 
            // ecran_PanneauConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lb_titre);
            this.Controls.Add(this.cb_Joueur);
            this.Controls.Add(this.cb_Fenetre);
            this.Name = "ecran_PanneauConfiguration";
            this.Size = new System.Drawing.Size(397, 167);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cb_Fenetre;
        private System.Windows.Forms.CheckBox cb_Joueur;
        private System.Windows.Forms.Label lb_titre;
    }
}
