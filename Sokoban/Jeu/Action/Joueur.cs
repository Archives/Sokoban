using System;
using System.Collections.Generic;
using System.Text;
using Sokoban.Jeu.Niveau;
using System.IO;
//using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace Sokoban.Jeu.Action
{
    [Serializable]
    public class Joueur //: ISerializable
    {
        /*
        * ------------------------------------------------------------------------------
        * --------- GESTION DE LA LISTE DES JOUEURS (static) ---------------------------
        * ------------------------------------------------------------------------------
        */
        /// <summary>
        /// La liste de joueurs avec leurs caratéristique
        /// </summary>
        private static Dictionary<String, Joueur> joueurs = new Dictionary<String, Joueur>();
        
        /// <summary>
        /// Ajouter un joueur à la liste
        /// </summary>
        /// <param name="nomJoueur">le nom du Joueur</param>
        /// <param name="joueur">les information du joueur</param>
        public static void AddJoueur (String nomJoueur, Joueur joueur) {
            joueurs.Add(nomJoueur, joueur);
        }

        /// <summary>
        /// Récuper la liste des joueurs connus
        /// </summary>
        /// <returns>La liste des joueurs</returns>
        public static List<String> nomsJoueurs()
        {
            List<String> nomsjoueurs = new List<String>();
            
            foreach (KeyValuePair<String, Joueur> joueur in joueurs)
            {
                nomsjoueurs.Add(joueur.Key);
            }
            return nomsjoueurs;
        }

        public static void charger()
        {
            String[] reps = Directory.GetFiles(@"Sauvegardes");
            
            //XmlSerializer deserisaliser = new XmlSerializer(typeof(Joueur));
            foreach (String fichier in reps) {
                try
                {                    
                    Stream oReadBinStream = File.Open(fichier, FileMode.Open);
                    BinaryFormatter oReadFormatter = new BinaryFormatter();
                    //Joueur joueur = new Joueur();
                    AddJoueur(fichier.Substring(12, fichier.Length-4-12), (Joueur)oReadFormatter.Deserialize(oReadBinStream));
                    oReadBinStream.Close ();
                    
                    /*StreamReader steam = new StreamReader(@"Sauvegardes\" + fichier);
                    Joueur joueur = (Joueur)deserisaliser.Deserialize(steam);
                    AddJoueur(fichier.Substring(0,-4), joueur) ;
                    steam.Close();*/
                }
                catch (Exception e){
                    System.Console.WriteLine(e.Message);
                }
            }
        }

        public static Joueur getJoueur(String nom)
        {
            Joueur joueur;
            if (joueurs.TryGetValue(nom, out joueur))
            {
                return joueur;
            } else {
                return null;
            }
        }

       /*
       * ------------------------------------------------------------------------------
       * --------- GESTION D'UN JOUEUR ------------------------------------------------
       * ------------------------------------------------------------------------------
       */
        /// <summary>
        ///  Nom du joueur 
        /// </summary>
        private String nomJoueur;
        public String NomJoueur
        {
            get
            {
                return nomJoueur;
            }
            set
            {
                if (value != null && !value.Equals(""))
                {
                    nomJoueur = value;
                }
                else throw new Exception("Nom de joueur non valide.");
            }
        }


        private String nomCourant;
        public String NomCourant
        {
            get
            {
                return nomCourant;
            }
            set
            {
                nomCourant = value;
            }
        }
        private int ligneCourant;
        public int LigneCourant
        {
            get
            {
                return ligneCourant;
            }
            set
            {
                ligneCourant = value;
            }
        }
        private int nivCourant;
        public int NivCourant
        {
            get
            {
                return nivCourant;
            }
            set
            {
                nivCourant = value;
            }
        }


        /// <summary>
        /// tableau de niveau
        /// null : niveau non effectué
        /// points : informations sur le niveau
        /// </summary>
        private Dictionary<String, JoueurNiveauStatuts> niveaux = new Dictionary<String, JoueurNiveauStatuts>();
       
        /// <summary>
        /// Créer un joueur sans aucun paramètre
        /// </summary>
        public Joueur() {}

        /// <summary>
        /// Créer un joueur à partir de rien si ce n'est un nom transmis
        /// </summary>
        /// <param name="nomJoueur">Nom du nouveau joueur</param>
        public Joueur(String nomJoueur) 
        {
            NomJoueur = nomJoueur;
        }

        public void Sauver()
        {
            try
            {
                
                Stream oWriteBinStream = File.Open(@"sauvegardes\" + NomJoueur + ".sav", FileMode.Create);
                BinaryFormatter oWriteFormatter = new BinaryFormatter ();
                oWriteFormatter.Serialize (oWriteBinStream, this);
                oWriteBinStream.Close (); 
                
                /*StreamWriter stream = new StreamWriter(@"sauvegardes\" + NomJoueur + ".xml");
                XmlSerializer serisaliser = new XmlSerializer(typeof(Joueur));
                serisaliser.Serialize(stream, this);
                stream.Close();*/
            }
            catch
            {
                System.Console.WriteLine("Sauvegarde Impossible.");
            }
        }


        public void AddScore(NivInfos nivInfo) {
            // niveau courant
            nomCourant = nivInfo.jeuNom;
            ligneCourant = nivInfo.jeuLigne;
            nivCourant = nivInfo.jeuNiveau;
            
            int deplacement = nivInfo.Deplacements;
            int temps = nivInfo.Temps;           

            String nom = nivInfo.jeuNom + "_" + nivInfo.jeuLigne + "-" + nivInfo.jeuNiveau;

            JoueurNiveauStatuts niveauRecords;
            if(niveaux.TryGetValue(nom, out niveauRecords)) {
                if (niveauRecords.MeilleurDeplacementEffectue[0] > temps)
                {
                    niveauRecords.MeilleurDeplacementEffectue = new int[] { temps, deplacement };
                }
                if (niveauRecords.MeilleurTempsEffectue[1] > deplacement)
                {
                    niveauRecords.MeilleurTempsEffectue = new int[] { temps, deplacement };
                }
            } else {
                niveaux.Add(nom, new JoueurNiveauStatuts(new int[] { temps, deplacement }));
            }

            Sauver();

        }

                
        /// <summary>
        /// Créer un tableau avec le nombre de niveaux possibles, mais vide.
        /// </summary>

        public void activer(string nomJoueur)
        {
            NomJoueur = nomJoueur;
        }

        
    }
}
