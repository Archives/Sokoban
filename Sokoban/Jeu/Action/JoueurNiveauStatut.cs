using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoban.Jeu.Action
{
    [Serializable]
    class JoueurNiveauStatuts
    {

        private int[] meilleurTempsEffectue = new int[2];
        public int[] MeilleurTempsEffectue
        {
            get
            {
                return meilleurTempsEffectue;
            }
            set
            {
                meilleurTempsEffectue = value;

            }

        }


        private int[] meilleurDeplacementEffectue = new int[2];
        public int[] MeilleurDeplacementEffectue
        {
            get
            {
                return meilleurDeplacementEffectue;
            }
            set
            {
                meilleurDeplacementEffectue = value;

            }

        }
        public JoueurNiveauStatuts() { }

        public JoueurNiveauStatuts(int[] effectue)
        {
            MeilleurTempsEffectue = effectue;
            MeilleurDeplacementEffectue = effectue;
        }        
    }
}
