using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace Sokoban.Jeu.Action
{
  
    [Serializable]
    public class Config
    {
        private String dernierJoueur;
        public String DernierJoueur
        {
            get
            {
                return dernierJoueur;
            }
            set
            {
                dernierJoueur = value;
            }
        }
        private int pos_x;
        public int Pos_x
        {
            get
            {
                return pos_x;
            }
            set
            {
                pos_x = value;
            }
        }
        private int pos_y;
        public int Pos_y
        {
            get
            {
                return pos_y;
            }
            set
            {
                pos_y = value;
            }
        }
        private int taille_x;
        public int Taille_x
        {
            get
            {
                return taille_x;
            }
            set
            {
                taille_x = value;
            }
        }
        private int taille_y;
        public int Taille_y
        {
            get
            {
                return taille_y;
            }
            set
            {
                taille_y = value;
            }
        }
        public bool saveDimensions;
        public bool saveDernierJoueur;

        private JeuSokoban sokoban;

        /// <summary>
        /// Configuration par d�faut
        /// </summary>
        public Config(JeuSokoban sokoban)
        {
           this.sokoban = sokoban;
        }
        public Config()
        {

        }


        /// <summary>
        /// Profil par d�faut
        /// </summary>
        public void defaut()
        {
            Pos_x = 50;
            Pos_y = 50;
            Taille_x = 300;
            Taille_y = 500;
            DernierJoueur = "";
        }

        /// <summary>
        /// Charger les informations de configuration continues dans le fichier XML de configuration
        /// </summary>
        public void chargerInfos () {
            XmlSerializer deserisaliser = new XmlSerializer(typeof(Config));
            try
            {
                StreamReader steam = new StreamReader(@"config.xml");
                Config config = (Config)deserisaliser.Deserialize(steam);
                charger(config);
                steam.Close();
            }
            catch
            {
                System.Console.WriteLine("Le fichier de configuration n'existe pas.");
                defaut();
                sauverInfos();
            }
        }

        /// <summary>
        /// Charger les informations de configuration r�cup�r�es
        /// </summary>
        /// <param name="config"></param>
        private void charger(Config config)
        {
            Pos_x = config.Pos_y;
            Pos_y = config.Pos_y;
            Taille_x = config.Taille_x;
            Taille_y = config.Taille_y;
            DernierJoueur = config.DernierJoueur;
            saveDernierJoueur = config.saveDernierJoueur;
            saveDimensions = config.saveDimensions;
        }

        /// <summary>
        /// Sauver les informations de configuration dans une fichier XML
        /// </summary>
        public void sauverInfos()
        {
            if (saveDimensions)
            {
                Pos_x = sokoban.Right;
                Pos_y = sokoban.Top;
                Taille_x = sokoban.Width;
                Taille_y = sokoban.Height;
            }
            if (!saveDernierJoueur) {
                DernierJoueur = "";
            }
            
            try
            {
                StreamWriter stream = new StreamWriter(@"config.xml");
                XmlSerializer serisaliser = new XmlSerializer(typeof(Config));
                serisaliser.Serialize(stream, this);
                stream.Close();
            }
            catch
            {
                System.Console.WriteLine("Sauvegarde Impossible.");
            }
        }
    }
}

