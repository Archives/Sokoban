using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sokoban.Jeu.Affichage;
using Sokoban.Jeu.Action;
using Sokoban.Jeu.Niveau;

namespace Sokoban.Jeu
{
    public partial class JeuSokoban : Form
    {
          

        /// <summary>
        /// nom des chemins où se trouves certains informations externes
        /// * à placer dans une fichier de configuration par la suite *
        /// </summary>
        public static string PATH_TERRAINS = "Terrains/";
        public static string PATH_SAUVEGARDES = "Sauvegardes/";
        
        /// <summary>
        /// Information de configuration
        /// </summary>
        private Config configuration;
        public Config Configuration {
            get
            {
                return configuration;
            }
            set
            {
                configuration = value;
            }
        }

        /// <summary>
        /// Démarrage avec lassement du menu principal
        /// </summary>
        public JeuSokoban()
        {
            KeyPreview = true;
            
            // recupération des information de lancement
            Configuration = new Config(this);
            Configuration.chargerInfos();

            // Récupérer les profils joureurs
            Joueur.charger();
            if (Configuration.saveDernierJoueur)
            {
                activerJoueur(Configuration.DernierJoueur);
            }

            // initalisation des éléments principaux : Barre de Menu et Panel
            InitializeComponent();
            // chargement de menu principal
            ecranEnCours = new ecran_MenuPrincipal(this);
            App.Controls.Add(ecranEnCours);
        }
      


       /*
       *     DU JOUEUR     ------------------------------------------------------------------------
       */

        /// <summary>
        /// Joueur en cours
        /// </summary>
        private Joueur joueurActif;
        public Joueur JoueurActif
        {
            get
            {
                return joueurActif;
            }
            set
            {
                joueurActif = value;
            }
        }
        /// <summary>
        /// Activation d'un joueur par le nom du joueur
        /// </summary>
        /// <param name="nomJoueur">Nom du joueur</param>
        public void activerJoueur(String nomJoueur)
        {
            Joueur joueur = Joueur.getJoueur(nomJoueur);
            JoueurActif = joueur;
            configuration.DernierJoueur = nomJoueur;
        }

        /// <summary>
        /// Si un jour est bien connecté. 
        /// </summary>
        /// <returns>Renvoie true il y a bien un jour connecté</returns>
        public bool siJoueur()
        {
            return (JoueurActif != null) ? true : false;
        }


        /*
        *     DES ECRANS   ------------------------------------------------------------------------
        */

        /// <summary>
        ///  nom des pages écrans
        /// </summary>
        public const int VIDE = -1;
        public const int MENU_PRINCIPAL = 0;
        public const int NOUVELLE_PARTIE = 1;
        public const int CARTE_NIVEAUX = 2;
        public const int IDENTIFICATION = 3;
        public const int NIVEAU = 4;
        public const int NIVEAUFINI = 5;
        public const int DIFFICULTE = 10;
        public const int CONFIGURATION = 20;
        public const int QUITTER = 999;

        /// <summary>
        /// page de retour
        /// </summary>
        private int ecranPrecedant;
        public int EcranPrecedant
        {
            get
            {
                return ecranPrecedant;
            }
            set
            {
                ecranPrecedant = value;

            }
        }

        /// <summary>
        /// page en suppend qui sera effectue après une action dérivée
        /// </summary>
        private int ecranSuivant;
        public int EcranSuivant
        {
            get
            {
                return ecranSuivant;
            }
            set
            {
                ecranSuivant = value;

            }
        }

        /// <summary>
        /// Écran actuellement affiché
        /// </summary>
        private UserControl ecranEnCours;
        public UserControl EcranEnCours
        {
            get
            {
                return ecranEnCours;
            }
            set
            {
                ecranEnCours = value;

            }
        }

       

        /// <summary>
        /// Changement de l'écran principal
        /// </summary>
        /// <param name="ecran">Identifiant de la page</param>
        public void ecran(int ecran)
        {
           

            App.Controls.Clear();
            switch (ecran)
            {
                case MENU_PRINCIPAL :
                    App.Controls.Add(new ecran_MenuPrincipal(this));
                    break;
                case NOUVELLE_PARTIE :
                    break;
                case CARTE_NIVEAUX :
                    break;
                case IDENTIFICATION :
                    App.Controls.Add(new ecran_Identification(this));
                    break;
                case DIFFICULTE :
                    App.Controls.Add(new ecran_Difficulte(this));
                    break;
                case NIVEAU :
                    App.Controls.Add(new ecran_Niveau(this, NiveauInfos));
                    break;
                case NIVEAUFINI:
                    App.Controls.Add(new ecran_NiveauFini(this));
                    break;
                case CONFIGURATION :
                    App.Controls.Add(new ecran_PanneauConfiguration(this));
                    break;
                case QUITTER :
                    Dispose();
                    break;
            }
        }
        
        /// <summary>
        /// Quitter le programme
        /// </summary>
        new public void Dispose()
        {
            Configuration.sauverInfos();
            Dispose(true);
        }

        /*
         *     GESTION DES NIVEAUX --------------------------------------------------------------------
         */

        /// <summary>
        /// Information sur le niveau
        /// </summary>
        private NivInfos niveauInfos;
        public NivInfos NiveauInfos
        {
            get
            {
                return niveauInfos;
            }
            set
            {
                niveauInfos = value;

            }
        }

        /// <summary>
        /// Activation d'un niveau avec les élements initialisés en le mettant dans la variable niveau
        /// </summary>
        /// <param name="ligne">numero de ligne</param>
        /// <param name="niveau">numero de niveau dans la ligne</param>
        public void activerNiveau(String nom, int ligne, int niveau)
        {
            niveauInfos = new NivInfos(nom, ligne, niveau);
        }

        /// <summary>
        /// Revoie les information nécessaire pour pouvoir afficher un niveau et 
        /// </summary>
        /// <param name="ligne">numero de ligne</param>
        /// <param name="niveau">numero de niveau dans la ligne</param>
        /// <returns></returns>
        public NivInfos informationsNiveau(String nom, int ligne, int niveau)
        {
            NivInfos niv = new NivInfos(nom, ligne, niveau);
            return niv;
        }


        /*
       *    Barre de menu --------------------------------------------------------------------
       */

        public void nouvellePartieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (siJoueur())
            {
                EcranPrecedant = MENU_PRINCIPAL;
                ecran(DIFFICULTE);
            }
            else
            {
                EcranPrecedant = MENU_PRINCIPAL;
                EcranSuivant = DIFFICULTE;
                ecran(IDENTIFICATION);
            }
        }

        public void sidentifierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ecran(JeuSokoban.IDENTIFICATION); 
        }

        public void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public void menuPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ecran(MENU_PRINCIPAL);
        }

        private void App_Paint(object sender, PaintEventArgs e)
        {

        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ecran(CONFIGURATION);
        }

        
    }
}