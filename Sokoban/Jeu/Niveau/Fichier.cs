using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace Sokoban.Jeu.Niveau
{
    public class Fichier
    {
        public bool isExist = false; 

        private int[,] niveau;
        private int[,] niveauElements;
        private int[] joueur = new int[2];

        /// <summary>
        /// Lit le ficher contenant les informations sur le niveau déterminer par les paramètres.
        /// </summary>
        /// <param name="nom">nom de la liste</param>
        /// <param name="ligne">numero de ligne</param>
        /// <param name="niveau">numero de niveau dans la ligne</param>
        public Fichier (String nom, int numLigne, int numNiveau)
        {
            try
            {
                TextReader xsb = new StreamReader(@"Niveaux\" + nom + @"\" + (numLigne - 1) + "-" + (numNiveau - 1) + ".xsb");
                // recherche la taille du niveau
                int largeur = 0;
                int lignes = 0;
                String ligne = "";
                List<String> nivLigne = new List<string>();
                while ((ligne = xsb.ReadLine()) != null)
                {
                    if (largeur < ligne.Length)
                    {
                        largeur = ligne.Length;
                    }
                    lignes++;
                    nivLigne.Add(ligne);
                }
                xsb.Close();

                niveau = new int[lignes, largeur];
                niveauElements = new int[lignes, largeur];

                int j = 0;
                foreach (String l in nivLigne)
                {
                    int i = 0;
                    IEnumerator<char> lEnum = l.GetEnumerator();
                    while (lEnum.MoveNext())
                    {
                        char ch = lEnum.Current;
                        if (ch.Equals('#'))
                        {
                            niveau[j, i] = 0;
                            niveauElements[j, i] = 0;
                        }
                        else if (ch.Equals('$'))
                        {
                            niveau[j, i] = 1;
                            niveauElements[j, i] = 1;
                        }
                        else if (ch.Equals('.'))
                        {
                            niveau[j, i] = 2;
                            niveauElements[j, i] = 0;
                        }
                        else if (ch.Equals('*'))
                        {
                            niveau[j, i] = 2;
                            niveauElements[j, i] = 1;
                        }
                        else if (ch.Equals('@'))
                        {
                            joueur[0] = j;
                            joueur[1] = i;
                            niveau[j, i] = 1;
                            niveauElements[j, i] = 0;
                        }
                        else if (ch.Equals('+'))
                        {
                            joueur[0] = j;
                            joueur[1] = i;
                            niveau[j, i] = 2;
                            niveauElements[j, i] = 0;
                        }
                        else
                        {
                            niveau[j, i] = 1;
                            niveauElements[j, i] = 0;
                        }
                        i++;
                    }
                    for (; i < largeur; i++)
                    {
                        niveau[j, i] = 1;
                    }
                    j++;
                }
                isExist = true;
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Le fichier « " + @"Niveaux\" + nom + @"\" + (numLigne - 1) + "-" + (numNiveau - 1) + ".xsb » n'existe pas ");
            }
         }

        public int[,] getNiveau()
        {
            return niveau;
        }
        public int[,] getNiveauElements()
        {
            return niveauElements;
        }
        public int[] getJoueur()
        {
            return joueur;
        }

    }
}
