using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoban.Jeu.Niveau
{
    public class NivInfos
    {
        public Fichier terrain;
        private String nom;
        public String Nom
        {
            get
            {
                return nom;
            }
        }
        public String jeuNom;
        public int jeuLigne;
        public int jeuNiveau;
        //public int nbrBlock;
        
        public NivInfos(String nom, int ligne, int niveau)
        {
            this.jeuNom = nom;
            this.jeuLigne = ligne;
            this.jeuNiveau = niveau;
            
            if (nom.Equals("defaut"))
            {
                this.nom = "Niveau " + ligne + "-" + niveau;
            } 
            else 
            {
                this.nom = nom + "  " + ligne + "-" + niveau;
            }          
            
            terrain = new Fichier(nom, ligne, niveau);
        }

        private int temps; 
        public int Temps
        {
            get
            {
                return temps;
            }
        }
        private int deplacements;
        public int Deplacements
        {
            get
            {
                return deplacements;
            }
        }

        public void setScore(int temps, int deplacements)
        {
            this.deplacements = deplacements;
            this.temps = temps;
        }
    }
}
